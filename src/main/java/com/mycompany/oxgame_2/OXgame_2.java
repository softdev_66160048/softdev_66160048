/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxgame_2;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class OXgame_2 {

    private static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-',}};
    private static char turn = 'O';
    private static Scanner sc = new Scanner(System.in);
    private static int row;
    private static int col;
    private static int count = 0;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            showBoard();
            showTurn();
            inputRowCol();
            //checkValidate();           
            if (isFinish()) {
                showBoard();
                showResult();
                break;
            }
            switchTurn();
        }
    }

    private static void printWelcome() { //method
        System.out.println("Welcome to XO game!!!");
    }

    private static void showBoard() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(board[r][c] + " ");
            }
            System.out.println();
        }
    }

    private static void showTurn() {
        System.out.println("Turn : " + turn);
    }

    private static void inputRowCol() {
        
        System.out.println("Please input row and column : ");        
        row = sc.nextInt();
        col = sc.nextInt();
        while (row < 1 || row > 3 || col < 1 || col > 3 || (board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O')) {
            System.out.println("Invalid input!!!! Please enter row and column again.: ");
            row = sc.nextInt();
            col = sc.nextInt();
        }
        System.out.println("Row : " + row + "\nColumn : " + col);
        board[row - 1][col - 1] = turn;             
        count++;
    }

    private static void switchTurn() {
        
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
        
    }

    private static boolean isFinish() {
        if (checkWin()) {
            return true;
        }
        if (checkFull()) {
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkFull() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        if (board[row - 1][1] == turn && board[row - 1][0] == turn && board[row - 1][2] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkCol() {
        if (board[0][col - 1] == turn && board[1][col - 1] == turn && board[2][col - 1] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkX() {
        if (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
            return true;
        }
        if (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
            return true;
        }
        return false;
    }

    private static void showResult() {
        if (checkWin()) {
            System.out.println(turn + " is winner!!!");
        }
        if (checkFull()) {
            System.out.println("Draw!!!");
        }
    }
}

